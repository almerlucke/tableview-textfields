//
//  AppDelegate.h
//  TableTextFieldTest
//
//  Created by aFrogleap on 10/31/12.
//  Copyright (c) 2012 farcoding. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
