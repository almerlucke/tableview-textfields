//
//  ViewController.m
//  TableTextFieldTest
//
//  Created by aFrogleap on 10/31/12.
//  Copyright (c) 2012 farcoding. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UITableViewCell *textFieldCell;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasHidden:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidHideNotification object:nil];
}


#pragma mark - Keyboard

- (void)keyboardWasShown:(NSNotification *)aNotification {
    CGRect keyboardBounds;
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    
    [[aNotification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardBounds];
    
    // convert window keyboard bounds to local bounds
    keyboardBounds = [window convertRect:keyboardBounds toView:self.view];
    // get intersection between bounds and local keyboard bounds
    keyboardBounds = CGRectIntersection(self.view.bounds, keyboardBounds);
    
    // calculate contentInset based on keyboard origin y and tableview top height
    CGFloat tableViewTopHeight = self.tableView.frame.origin.y + self.tableView.frame.size.height;
    CGFloat contentInsetBottom = tableViewTopHeight - keyboardBounds.origin.y;
    if (contentInsetBottom > 0) {
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, contentInsetBottom, 0);
    }
    
    // calculate contentOffset based on visibility of the cell that holds the first responder
    UITableViewCell *cell = [self tableViewCellContainingFirstResponderInView:self.tableView];
    if (cell) {
        CGRect cellRect = [self.tableView rectForRowAtIndexPath:[self.tableView indexPathForCell:cell]];
        cellRect = [self.view convertRect:cellRect fromView:self.tableView];
        CGFloat cellTopHeight = cellRect.origin.y + cellRect.size.height + 20; // add 20 for some extra space between keyboard and cell
        CGFloat contentOffsetY = cellTopHeight - keyboardBounds.origin.y;
        
        if (contentOffsetY > 0) {
            CGPoint contentOffset = CGPointMake(0, self.tableView.contentOffset.y + contentOffsetY); // add to current content offset
            [UIView animateWithDuration:0.1 animations:^{
                self.tableView.contentOffset = contentOffset;
            }];
        }
    }
}

- (void)keyboardWasHidden:(NSNotification *)aNotification {
    [UIView animateWithDuration:0.1 animations:^{
        self.tableView.contentInset = UIEdgeInsetsZero;
    }];
}

#pragma mark - Utils

- (BOOL)isFirstResponderSubviewOfView:(UIView *)view
{
    for (id subview in view.subviews) {
        BOOL containsFirstResponder = ([subview isFirstResponder] || [self isFirstResponderSubviewOfView:subview]);
        if (containsFirstResponder) return YES;
    }
    
    return NO;
}

/*
 *  Discussion: maybe we have to look for any subview of tableview for first responder
 *  instead of only looking for cells containing it. what if somebody puts a textfield in the
 *  tableview footer view?
 */
- (UITableViewCell *)tableViewCellContainingFirstResponderInView:(UIView *)view
{
    for (id subview in view.subviews) {
        if ([subview isKindOfClass:[UITableViewCell class]]) {
            if ([self isFirstResponderSubviewOfView:subview]) {
                return subview;
            }
        } else {
            UITableViewCell *cell = [self tableViewCellContainingFirstResponderInView:subview];
            if (cell) {
                return cell;
            }
        }
    }
    
    return nil;
}

#pragma mark - UITextField

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - UITableView

- (UITableViewCell *)textFieldCell
{
    if (!_textFieldCell) {
        _textFieldCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        _textFieldCell.selectionStyle = UITableViewCellSelectionStyleNone;
        _textField = [[UITextField alloc] initWithFrame:CGRectMake(10, 10, 280, 20)];
        _textField.font = [UIFont boldSystemFontOfSize:18.0];
        _textField.placeholder = @"Check it out!";
        _textField.delegate = self;
        [_textFieldCell.contentView addSubview:_textField];
    }
    
    return _textFieldCell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numRows = 0;
    
    if (section == 0) {
        numRows = 10;
    } else if (section == 1) {
        numRows = 1;
    } else if (section == 2) {
        numRows = 6;
    }
    
    return numRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"cellID";
    UITableViewCell *cell = nil;
    
    if (indexPath.section == 1) return self.textFieldCell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    cell.textLabel.text = @"Check";
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 1) {
        [self.textField becomeFirstResponder];
    }
    
    NSLog(@"select row %d", indexPath.row);
}



@end
